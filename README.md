# Introduction

This is a template for getting started with git, learning basic machine learning concepts, and practicing basic python skills. And then, you'll learn how to use server and start to work on a project.

## Getting started

First of all, you'll need to prepare the environment and a git account before your practice:

Git:
1.  Create Git ([GitHub](https://github.com), [GitLab](https://gitlab.com)) account
2.  Install local GUI ([Sourcetree](https://www.sourcetreeapp.com))

Python:
1.  Install Python IDE ([PyCharm](https://www.jetbrains.com/pycharm/download/#section=mac)): community version
2.  Install Conda ([Anaconda](https://www.anaconda.com/distribution/#download-section)/Miniconda): 64-bit (.pkg installer)

Use Anaconda to create virtual environment with python version 3.7 and name the environment "practice3.7". This env will be used for the following practices.

## Practices

You'll complete the following tasks (issues) in order:

 1.  Practice git
 2.  Learn basic NN and create a function
 3.  Practice server

usually the above task takes can be completed within a week.

Then you can work on the following tasks simultaneously:

 - Learn ML models
 - Start a project

## Reference links

- [為你自己學 Git](https://gitbook.tw)

